/*
//crud

/!*const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient*!/

const { MongoClient, ObjectID} = require('mongodb');

const connectionURL = "mongodb://127.0.0.1:27017";
const database = "first-app-db";

/!*const id = new ObjectID();
console.log(id);*!/

MongoClient.connect(connectionURL, {useUnifiedTopology: true, useNewUrlParser: true }, (error, conn) => {
    if (error){
        return console.log('Unable to connect to database');
    }

    const db = conn.db(database);

    //console.log('Connected successfully');

    db.collection('users').insertMany({
        name: 'Tanjil Khijli',
        designation: 'Software Engineer'
    })

    db.collection('users').insertMany([
        {
            name: 'Jhon Deo',
            role: 'Khijli Man',
            age: '999'
        },
        {
            name: "jhon Smith",
            role: "Khijli",
            age: "999"
        }
    ], (error, result) => {
        if (error){
            return console.log("Unable to insert data")
        }
        console.log(result.ops)
    })

    db.collection('tasks').insert([
        {
            title: 'This your first task',
            completed: true,
        },{
            title: 'This your second task',
            completed: false,
        },{
            title: 'This your third task',
            completed: true,
        },
    ], (error, result) => {
        if (error){
            return console.log('Unable to insert data')
        }
        console.log(result.ops)
    })

    db.collection('users').findOne({ name: 'Tanjil Khijli'}, (error, user) => {
        if (error){
            return console.log('Unable to get data');
        }

        console.log(user)
    })

    db.collection('users').find({age:'999'}).toArray((error, users) => {
        console.log(users)
    });

    const updatePromise = db.collection('users').updateOne({
        _id: new ObjectID("5dedeb0c425b052eacf4549e")
    }, {
        $set: {
            name: 'Tanjil Hasan',
            designation: 'Software Engineer (Laravel)',
            role: 'Developer',
            age: 25
        }
    });

    updatePromise.then((result) => {
        console.log(result);
    }).catch((error) => {
        console.log(error)
    })

});*/
